package com.kata.hexago;

import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.noClasses;

import com.tngtech.archunit.core.domain.JavaClasses;
import com.tngtech.archunit.core.importer.ImportOption;
import com.tngtech.archunit.junit.AnalyzeClasses;
import com.tngtech.archunit.junit.ArchTest;

/**
 * Arch unit testing, to enforce some hexagonal architecture rules.
 *
 * For more info on how to write them: https://www.archunit.org/userguide/html/000_Index.html
 */
@AnalyzeClasses(packages = "com.kata.hexago", importOptions = ImportOption.DoNotIncludeTests.class)
public class ArchitectureRulesTest {

    @ArchTest
    public static void domain_should_not_call_infrastructure(JavaClasses classes) {
        noClasses()
            .that()
            .resideInAPackage("..domain..")
            .should()
            .dependOnClassesThat()
            .resideInAPackage("..infrastructure..")
            .check(classes);
    }

    @ArchTest
    public static void application_should_not_call_infrastructure(JavaClasses classes) {
        noClasses()
            .that()
            .resideInAPackage("..application..")
            .should()
            .dependOnClassesThat()
            .resideInAPackage("..infrastructure..")
            .check(classes);
    }
}
