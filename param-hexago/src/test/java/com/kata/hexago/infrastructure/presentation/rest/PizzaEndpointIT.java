package com.kata.hexago.infrastructure.presentation.rest;

import static io.restassured.RestAssured.given;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.mockito.Mockito.when;

import com.kata.hexago.domain.service.PizzaServicePort;
import com.kata.hexago.infrastructure.Stubs;
import com.kata.hexago.infrastructure.presentation.rest.dto.PizzaDoughTypePriceDto;
import io.vavr.control.Option;
import java.net.MalformedURLException;
import java.net.URI;
import org.apache.http.HttpStatus;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.mock.mockito.MockBean;

class PizzaEndpointIT extends AbstractEndpointIT {
    URI pizzaUri;

    @MockBean
    PizzaServicePort pizzaService;

    @BeforeEach
    void setUp() throws MalformedURLException {
        super.setUp();
        pizzaUri = baseApiUri.resolve(baseApiUri.getPath() + "/pizza");
    }

    @Test
    void should_return_a_404_error_when_pizza_dough_type_was_not_found() {
        // Given
        when(pizzaService.getPizzaDoughTypePricePerKilo(Stubs.pizzaDoughTypeUuidOne)).thenReturn(Option.none());

        // When Then
        given()
            .baseUri(pizzaUri.toString())
            .pathParam("pizzaDoughTypeId", Stubs.pizzaDoughTypeUuidOne)
            .get("/dough-type/{pizzaDoughTypeId}/price")
            .then()
            .statusCode(HttpStatus.SC_NOT_FOUND);
    }

    @Test
    void should_return_pizza_dough_type_price_per_kilo() {
        // Given
        when(pizzaService.getPizzaDoughTypePricePerKilo(Stubs.pizzaDoughTypeUuidOne))
            .thenReturn(Option.of(Stubs.pizzaDoughTypePriceOne));

        // When
        PizzaDoughTypePriceDto pizzaDoughTypePricePriceReturned = given()
            .baseUri(pizzaUri.toString())
            .pathParam("pizzaDoughTypeId", Stubs.pizzaDoughTypeUuidOne)
            .get("/dough-type/{pizzaDoughTypeId}/price")
            .then()
            .statusCode(HttpStatus.SC_OK)
            .extract()
            .as(PizzaDoughTypePriceDto.class);

        // Then
        assertThat(pizzaDoughTypePricePriceReturned).isNotNull();
        assertAll(
            () ->
                assertThat(pizzaDoughTypePricePriceReturned.getPizzaDoughTypeId())
                    .isEqualByComparingTo(Stubs.pizzaDoughTypeUuidOne),
            () -> assertThat(pizzaDoughTypePricePriceReturned.getPricePerKilo()).isEqualByComparingTo(Stubs.priceOne)
        );
    }
}
