package com.kata.hexago.infrastructure.persistence.repository;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.assertAll;

import com.kata.hexago.infrastructure.Stubs;
import com.kata.hexago.infrastructure.repository.PizzaDoughTypeRepository;
import org.assertj.vavr.api.VavrAssertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
class PizzaDoughTypeDoughTypeRepositoryTest {
    @InjectMocks
    PizzaDoughTypeRepository pizzaDoughTypeRepository;

    @Test
    void should_return_empty_if_id_is_not_found() {
        VavrAssertions
            .assertThat(pizzaDoughTypeRepository.getPizzaDoughTypePricePerKilo(Stubs.pizzaDoughTypeUuidNotFound))
            .isEmpty();
    }

    @Test
    void should_return_price_if_id_is_found() {
        VavrAssertions
            .assertThat(pizzaDoughTypeRepository.getPizzaDoughTypePricePerKilo(Stubs.pizzaDoughTypeUuidOne))
            .isDefined();
    }
}
