package com.kata.hexago.infrastructure.persistence.repository;

import static com.kata.hexago.infrastructure.Stubs.*;

import com.kata.hexago.domain.entity.IngredientPrice;
import com.kata.hexago.infrastructure.repository.IngredientRepository;
import io.vavr.control.Option;
import org.assertj.vavr.api.VavrAssertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class IngredientRepositoryTest {
    @InjectMocks
    IngredientRepository ingredientRepository;

    /**
     * FIXME: temporary test, should be deleted once actual database is configured
     */
    @Test
    void should_return_ingredients_prices_for_existant_uuids_correctly() {
        // When
        Option<IngredientPrice> ingredientPriceOne = ingredientRepository.getIngredientPricePerKilo(ingredientUuidOne);
        Option<IngredientPrice> ingredientPriceTwo = ingredientRepository.getIngredientPricePerKilo(ingredientUuidTwo);

        // Then
        VavrAssertions.assertThat(ingredientPriceOne).isDefined();
        VavrAssertions.assertThat(ingredientPriceTwo).isDefined();
    }

    /**
     * FIXME: temporary test, should be deleted once actual database is configured
     */
    @Test
    void should_not_return_ingredients_prices_for_absent_uuids_correctly() {
        // When
        Option<IngredientPrice> ingredientPriceOne = ingredientRepository.getIngredientPricePerKilo(
            ingredientUuidNotFound
        );

        // Then
        VavrAssertions.assertThat(ingredientPriceOne).isEmpty();
    }
}
