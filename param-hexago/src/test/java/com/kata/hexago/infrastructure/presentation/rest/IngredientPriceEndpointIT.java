package com.kata.hexago.infrastructure.presentation.rest;

import static io.restassured.RestAssured.given;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.mockito.Mockito.when;

import com.kata.hexago.domain.service.IngredientServicePort;
import com.kata.hexago.infrastructure.Stubs;
import com.kata.hexago.infrastructure.presentation.rest.dto.IngredientPriceDto;
import io.vavr.control.Option;
import java.net.MalformedURLException;
import java.net.URI;
import org.apache.http.HttpStatus;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.mock.mockito.MockBean;

class IngredientPriceEndpointIT extends AbstractEndpointIT {
    URI ingredientUri;

    @MockBean
    IngredientServicePort ingredientService;

    @BeforeEach
    void setUp() throws MalformedURLException {
        super.setUp();
        ingredientUri = baseApiUri.resolve(baseApiUri.getPath() + "/ingredient");
    }

    @Test
    void should_return_ingredient_price_per_kilo() {
        // Given
        when(ingredientService.getIngredientPricePerKilo(Stubs.ingredientUuidOne))
            .thenReturn(Option.of(Stubs.ingredientPriceOne));

        // When
        IngredientPriceDto ingredientPriceReturned = given()
            .baseUri(ingredientUri.toString())
            .pathParam("ingredientId", Stubs.ingredientUuidOne)
            .get("/{ingredientId}/price")
            .then()
            .statusCode(HttpStatus.SC_OK)
            .extract()
            .as(IngredientPriceDto.class);

        // Then
        assertAll(
            "Ingredient price returned should match correct values",
            () -> assertThat(ingredientPriceReturned.getIngredientId()).isEqualTo(Stubs.ingredientUuidOne),
            () -> assertThat(ingredientPriceReturned.getPricePerKilo()).isEqualTo(Stubs.priceOne)
        );
    }

    @Test
    void should_return_not_found_error_when_ingredient_does_not_exists() {
        // Given
        when(ingredientService.getIngredientPricePerKilo(Stubs.ingredientUuidNotFound)).thenReturn(Option.none());

        // When Then
        given()
            .baseUri(ingredientUri.toString())
            .pathParam("ingredientId", Stubs.ingredientUuidNotFound)
            .get("/{ingredientId}/price")
            .then()
            .statusCode(HttpStatus.SC_NOT_FOUND);
    }
}
