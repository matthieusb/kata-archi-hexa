package com.kata.hexago.infrastructure.presentation.rest.dto;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import com.kata.hexago.domain.entity.PizzaDoughTypePrice;
import com.kata.hexago.infrastructure.Stubs;
import org.junit.jupiter.api.Test;

class PizzaDoughTypePriceDtoTest {

    @Test
    void should_create_dto_from_domain() {
        // Given
        PizzaDoughTypePrice pizzaDoughTypePrice = mock(PizzaDoughTypePrice.class);
        when(pizzaDoughTypePrice.getPizzaDoughTypeid()).thenReturn(Stubs.pizzaDoughTypeUuidOne);
        when(pizzaDoughTypePrice.getPricePerKilo()).thenReturn(Stubs.priceOne);

        // When
        PizzaDoughTypePriceDto pizzaDoughTypePriceDtoReturned = PizzaDoughTypePriceDto.from(pizzaDoughTypePrice);

        // Then
        assertThat(pizzaDoughTypePriceDtoReturned).isNotNull();
        assertAll(
            () ->
                assertThat(pizzaDoughTypePriceDtoReturned.getPizzaDoughTypeId())
                    .isEqualByComparingTo(Stubs.pizzaDoughTypeUuidOne),
            () -> assertThat(pizzaDoughTypePriceDtoReturned.getPricePerKilo()).isEqualByComparingTo(Stubs.priceOne)
        );
    }
}
