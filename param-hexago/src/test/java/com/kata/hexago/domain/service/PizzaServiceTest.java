package com.kata.hexago.domain.service;

import static org.mockito.Mockito.when;

import com.kata.hexago.domain.entity.PizzaDoughTypePrice;
import com.kata.hexago.domain.repository.PizzaDoughTypeRepositoryPort;
import com.kata.hexago.infrastructure.Stubs;
import io.vavr.control.Option;
import org.assertj.vavr.api.VavrAssertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
class PizzaServiceTest {
    @Mock
    PizzaDoughTypeRepositoryPort pizzaDoughTypeRepository;

    @InjectMocks
    PizzaService pizzaService;

    @Test
    void should_not_get_pizza_dough_type_price() {
        // Given
        when(pizzaDoughTypeRepository.getPizzaDoughTypePricePerKilo(Stubs.pizzaDoughTypeUuidOne))
            .thenReturn(Option.none());

        // When Then
        VavrAssertions.assertThat(pizzaService.getPizzaDoughTypePricePerKilo(Stubs.pizzaDoughTypeUuidOne)).isEmpty();
    }

    @Test
    void should_get_pizza_dough_type_price() {
        // Given
        when(pizzaDoughTypeRepository.getPizzaDoughTypePricePerKilo(Stubs.pizzaDoughTypeUuidOne))
            .thenReturn(Option.of(Stubs.pizzaDoughTypePriceOne));

        // When
        Option<PizzaDoughTypePrice> pizzaDoughTypePriceReturned = pizzaService.getPizzaDoughTypePricePerKilo(
            Stubs.pizzaDoughTypeUuidOne
        );

        // Then
        VavrAssertions.assertThat(pizzaDoughTypePriceReturned).isDefined();
    }
}
