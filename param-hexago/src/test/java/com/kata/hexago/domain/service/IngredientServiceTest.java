package com.kata.hexago.domain.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import com.kata.hexago.domain.entity.IngredientPrice;
import com.kata.hexago.domain.repository.IngredientRepositoryPort;
import io.vavr.control.Option;
import java.util.UUID;
import org.assertj.vavr.api.VavrAssertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
class IngredientServiceTest {
    @Mock
    IngredientRepositoryPort ingredientRepository;

    @InjectMocks
    IngredientService ingredientService;

    @Test
    void should_get_ingredient_price_per_kilo() {
        // Given
        UUID uuid = UUID.randomUUID();

        IngredientPrice ingredientPrice = mock(IngredientPrice.class);
        when(ingredientPrice.getIngredientId()).thenReturn(uuid);
        when(ingredientPrice.getPricePerKilo()).thenReturn(2.0D);

        when(ingredientRepository.getIngredientPricePerKilo(uuid)).thenReturn(Option.of(ingredientPrice));

        // When
        Option<IngredientPrice> ingredientPriceReturned = ingredientService.getIngredientPricePerKilo(uuid);

        // Then
        VavrAssertions.assertThat(ingredientPriceReturned).isDefined();
        assertAll(
            "Ingredient price values returned match",
            () -> assertThat(ingredientPriceReturned.get().getIngredientId()).isEqualTo(uuid),
            () -> assertThat(ingredientPriceReturned.get().getPricePerKilo()).isEqualTo(2.0D)
        );
    }

    @Test
    void should_not_get_ingredient_price_per_kilo() {
        // Given
        UUID uuid = UUID.randomUUID();

        when(ingredientRepository.getIngredientPricePerKilo(uuid)).thenReturn(Option.none());

        // When
        Option<IngredientPrice> ingredientPriceReturned = ingredientService.getIngredientPricePerKilo(uuid);

        // Then
        VavrAssertions.assertThat(ingredientPriceReturned).isEmpty();
    }
}
