package com.kata.hexago.infrastructure.presentation.rest.dto;

import com.kata.hexago.domain.entity.IngredientPrice;
import java.util.UUID;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
@AllArgsConstructor
public class IngredientPriceDto {
    private UUID ingredientId;
    private Double pricePerKilo;

    public static IngredientPriceDto from(IngredientPrice ingredientPrice) {
        return new IngredientPriceDto(ingredientPrice.getIngredientId(), ingredientPrice.getPricePerKilo());
    }
}
