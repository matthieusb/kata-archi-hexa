package com.kata.hexago.infrastructure;

import com.kata.hexago.domain.entity.IngredientPrice;
import com.kata.hexago.domain.entity.PizzaDoughTypePrice;
import java.util.UUID;

public class Stubs {
    // -- General
    public static final Double priceOne = 1.0D;
    public static final Double priceTwo = 2.0D;

    // -- Ingredients
    public static final UUID ingredientUuidOne = UUID.fromString("dfb767a7-e193-4718-8a63-1bb9941570a0");
    public static final UUID ingredientUuidTwo = UUID.fromString("dfb767a8-e193-4718-8a63-1bb9941570a0");
    public static final UUID ingredientUuidNotFound = UUID.fromString("dfb267a7-e193-4718-8a63-1bb994151222");

    public static final IngredientPrice ingredientPriceOne = new IngredientPrice(ingredientUuidOne, priceOne);
    public static final IngredientPrice ingredientPriceTwo = new IngredientPrice(ingredientUuidTwo, priceTwo);

    // -- Pizza Dough Type
    public static final UUID pizzaDoughTypeUuidOne = UUID.fromString("dfb767a7-e193-4718-8b63-1bb9941570a0");

    public static final PizzaDoughTypePrice pizzaDoughTypePriceOne = new PizzaDoughTypePrice(
        pizzaDoughTypeUuidOne,
        priceOne
    );

    public static final UUID pizzaDoughTypeUuidNotFound = UUID.fromString("dfb767a7-e192-4818-8b63-1bb9941570a1");
}
