package com.kata.hexago.infrastructure.repository;

import com.kata.hexago.domain.entity.PizzaDoughTypePrice;
import com.kata.hexago.domain.repository.PizzaDoughTypeRepositoryPort;
import com.kata.hexago.infrastructure.Stubs;
import com.patternity.annotation.designpattern.Adapter;
import io.vavr.control.Option;
import java.util.UUID;
import org.springframework.stereotype.Repository;

@Adapter // secondary
@Repository
public class PizzaDoughTypeRepository implements PizzaDoughTypeRepositoryPort {

    // TODO Inject actual JpaRepository once ready (See issue #14)

    @Override
    public Option<PizzaDoughTypePrice> getPizzaDoughTypePricePerKilo(UUID pizzaDoughTypeId) {
        if (Stubs.pizzaDoughTypeUuidOne.equals(pizzaDoughTypeId)) {
            return Option.of(Stubs.pizzaDoughTypePriceOne);
        }

        return Option.none();
    }
}
