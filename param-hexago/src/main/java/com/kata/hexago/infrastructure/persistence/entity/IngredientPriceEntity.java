package com.kata.hexago.infrastructure.persistence.entity;

import java.util.UUID;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@AllArgsConstructor
@NoArgsConstructor
public class IngredientPriceEntity {
    // TODO Complete this with jpa annotations once possible (See issue #14)

    private UUID id;
    private Double pricePerKilo;
}
