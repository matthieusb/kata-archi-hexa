package com.kata.hexago.infrastructure.presentation.rest;

import com.kata.hexago.domain.service.PizzaServicePort;
import com.kata.hexago.infrastructure.presentation.rest.dto.PizzaDoughTypePriceDto;
import com.patternity.annotation.designpattern.Adapter;
import io.vavr.control.Option;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@Adapter // primary
@RestController
@RequiredArgsConstructor
public class PizzaEndpoint {
    private final PizzaServicePort pizzaService;

    @GetMapping("pizza/dough-type/{id}/price")
    public ResponseEntity<PizzaDoughTypePriceDto> getPizzaDoughTypePricePerKilo(
        @PathVariable("id") UUID pizzaDoughTypeId
    ) {
        Option<PizzaDoughTypePriceDto> pizzaDoughTypePrice = pizzaService
            .getPizzaDoughTypePricePerKilo(pizzaDoughTypeId)
            .map(PizzaDoughTypePriceDto::from);

        return ResponseEntity.of(pizzaDoughTypePrice.toJavaOptional());
    }
}
