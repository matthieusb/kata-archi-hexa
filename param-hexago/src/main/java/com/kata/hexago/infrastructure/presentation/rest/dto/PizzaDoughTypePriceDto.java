package com.kata.hexago.infrastructure.presentation.rest.dto;

import com.kata.hexago.domain.entity.PizzaDoughTypePrice;
import java.util.UUID;

public class PizzaDoughTypePriceDto {
    UUID pizzaDoughTypeId;
    Double pricePerKilo;

    public static PizzaDoughTypePriceDto from(PizzaDoughTypePrice pizzaDoughTypePrice) {
        return new PizzaDoughTypePriceDto(
            pizzaDoughTypePrice.getPizzaDoughTypeid(),
            pizzaDoughTypePrice.getPricePerKilo()
        );
    }

    private PizzaDoughTypePriceDto(UUID pizzaDoughTypeId, Double pricePerKilo) {
        this.pizzaDoughTypeId = pizzaDoughTypeId;
        this.pricePerKilo = pricePerKilo;
    }

    public UUID getPizzaDoughTypeId() {
        return pizzaDoughTypeId;
    }

    public Double getPricePerKilo() {
        return pricePerKilo;
    }
}
