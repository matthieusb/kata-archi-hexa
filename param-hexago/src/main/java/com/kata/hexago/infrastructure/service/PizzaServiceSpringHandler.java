package com.kata.hexago.infrastructure.service;

import com.kata.hexago.domain.repository.PizzaDoughTypeRepositoryPort;
import com.kata.hexago.domain.service.PizzaService;
import org.springframework.stereotype.Service;

@Service
public class PizzaServiceSpringHandler extends PizzaService {

    public PizzaServiceSpringHandler(PizzaDoughTypeRepositoryPort pizzaDoughTypeRepository) {
        super(pizzaDoughTypeRepository);
    }
}
