package com.kata.hexago.infrastructure.repository;

import static com.kata.hexago.infrastructure.Stubs.ingredientUuidOne;
import static com.kata.hexago.infrastructure.Stubs.ingredientUuidTwo;

import com.kata.hexago.domain.entity.IngredientPrice;
import com.kata.hexago.domain.repository.IngredientRepositoryPort;
import com.patternity.annotation.designpattern.Adapter;
import io.vavr.control.Option;
import java.util.UUID;
import org.springframework.stereotype.Repository;

@Adapter // secondary
@Repository
public class IngredientRepository implements IngredientRepositoryPort {

    // TODO Inject actual JpaRepository once ready (See issue #14)

    @Override
    public Option<IngredientPrice> getIngredientPricePerKilo(UUID ingredientId) {
        if (ingredientId.equals(ingredientUuidOne)) {
            return Option.of(new IngredientPrice(ingredientUuidOne, 2.0D));
        }

        if (ingredientId.equals(ingredientUuidTwo)) {
            return Option.of(new IngredientPrice(ingredientUuidTwo, 4.3D));
        }

        return Option.none();
    }
}
