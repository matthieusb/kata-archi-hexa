package com.kata.hexago.infrastructure.presentation.rest;

import com.kata.hexago.domain.service.IngredientServicePort;
import com.kata.hexago.infrastructure.presentation.rest.dto.IngredientPriceDto;
import com.patternity.annotation.designpattern.Adapter;
import io.vavr.control.Option;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@Adapter // primary
@RestController
@RequiredArgsConstructor
public class IngredientPriceEndpoint {
    private final IngredientServicePort ingredientService;

    @GetMapping("/ingredient/{id}/price")
    public ResponseEntity<IngredientPriceDto> getIngredientPricePerKilo(@PathVariable("id") UUID ingredientId) {
        Option<IngredientPriceDto> ingredientPrice = ingredientService
            .getIngredientPricePerKilo(ingredientId)
            .map(IngredientPriceDto::from);

        return ResponseEntity.of(ingredientPrice.toJavaOptional());
    }
}
