package com.kata.hexago.domain.service;

import com.kata.hexago.domain.entity.IngredientPrice;
import com.patternity.annotation.ddd.stereotype.Port;
import io.vavr.control.Option;
import java.util.UUID;

@Port // primary
public interface IngredientServicePort {
    Option<IngredientPrice> getIngredientPricePerKilo(UUID searchedIngredientId);
}
