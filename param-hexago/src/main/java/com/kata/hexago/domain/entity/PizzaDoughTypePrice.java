package com.kata.hexago.domain.entity;

import java.util.UUID;

public class PizzaDoughTypePrice {
    private final UUID pizzaDoughTypeid;
    private final Double pricePerKilo;

    // TODO Add static factory method and hide constructor

    // TODO Add some validation rules
    public PizzaDoughTypePrice(UUID pizzaDoughTypeid, Double pricePerKilo) {
        this.pizzaDoughTypeid = pizzaDoughTypeid;
        this.pricePerKilo = pricePerKilo;
    }

    public UUID getPizzaDoughTypeid() {
        return pizzaDoughTypeid;
    }

    public Double getPricePerKilo() {
        return pricePerKilo;
    }
}
