package com.kata.hexago.domain.service;

import com.kata.hexago.domain.entity.PizzaDoughTypePrice;
import com.kata.hexago.domain.repository.PizzaDoughTypeRepositoryPort;
import io.vavr.control.Option;
import java.util.UUID;

public class PizzaService implements PizzaServicePort {
    PizzaDoughTypeRepositoryPort pizzaDoughTypeRepository;

    public PizzaService(PizzaDoughTypeRepositoryPort pizzaDoughTypeRepository) {
        this.pizzaDoughTypeRepository = pizzaDoughTypeRepository;
    }

    @Override
    public Option<PizzaDoughTypePrice> getPizzaDoughTypePricePerKilo(UUID pizzaDoughTypeId) {
        return pizzaDoughTypeRepository.getPizzaDoughTypePricePerKilo(pizzaDoughTypeId);
    }
}
