package com.kata.hexago.domain.entity;

import java.util.UUID;

public class IngredientPrice {
    private final UUID ingredientId;
    private final Double pricePerKilo;

    // TODO Add some validation rules
    public IngredientPrice(UUID ingredientId, Double pricePerKilo) {
        this.ingredientId = ingredientId;
        this.pricePerKilo = pricePerKilo;
    }

    public UUID getIngredientId() {
        return ingredientId;
    }

    public Double getPricePerKilo() {
        return pricePerKilo;
    }
}
