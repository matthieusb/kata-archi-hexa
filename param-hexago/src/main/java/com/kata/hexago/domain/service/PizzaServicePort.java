package com.kata.hexago.domain.service;

import com.kata.hexago.domain.entity.PizzaDoughTypePrice;
import com.patternity.annotation.ddd.stereotype.Port;
import io.vavr.control.Option;
import java.util.UUID;

@Port // primary
public interface PizzaServicePort {
    Option<PizzaDoughTypePrice> getPizzaDoughTypePricePerKilo(UUID pizzaDoughTypeId);
}
