package com.kata.hexago.domain.repository;

import com.kata.hexago.domain.entity.IngredientPrice;
import com.patternity.annotation.ddd.stereotype.Port;
import io.vavr.control.Option;
import java.util.UUID;

@Port // secondary
public interface IngredientRepositoryPort {
    Option<IngredientPrice> getIngredientPricePerKilo(UUID ingredientId);
}
