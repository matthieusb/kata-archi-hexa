package com.kata.hexago.domain.service;

import com.kata.hexago.domain.entity.IngredientPrice;
import com.kata.hexago.domain.repository.IngredientRepositoryPort;
import io.vavr.control.Option;
import java.util.UUID;

public class IngredientService implements IngredientServicePort {
    private final IngredientRepositoryPort ingredientRepository;

    public IngredientService(IngredientRepositoryPort ingredientRepository) {
        this.ingredientRepository = ingredientRepository;
    }

    @Override
    public Option<IngredientPrice> getIngredientPricePerKilo(UUID searchedIngredientId) {
        return ingredientRepository.getIngredientPricePerKilo(searchedIngredientId);
    }
}
