package com.kata.hexago.domain.repository;

import com.kata.hexago.domain.entity.PizzaDoughTypePrice;
import com.patternity.annotation.ddd.stereotype.Port;
import io.vavr.control.Option;
import java.util.UUID;

@Port // Secondary
public interface PizzaDoughTypeRepositoryPort {
    Option<PizzaDoughTypePrice> getPizzaDoughTypePricePerKilo(UUID pizzaDoughTypeId);
}
