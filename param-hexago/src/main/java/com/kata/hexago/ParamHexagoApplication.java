package com.kata.hexago;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ParamHexagoApplication {

    public static void main(String[] args) {
        SpringApplication.run(ParamHexagoApplication.class, args);
    }
}
