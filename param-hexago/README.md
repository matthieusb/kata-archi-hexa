# kata-archi-hexa, param micro-service

## Project description

This project aims to demonstrate hexagonal architecture usage.
This is the parameter micro-service, it is mostly a data referential service which will just request a database to get some parametrized value and return them to the main micro-service.

## Usage

## Development

### Pre-requisites

* Java 11
* Maven 3.5 or higher (You can use the wrapper)
* Python 3 (for maven pre-commit plugin usage)

### Building/Launching/Testing

#### Build

Build the project and its tests:

```bash
mvn clean test-compile
```

#### Launch / Swagger UI / H2 Console

Launch the main rest api:
```bash
mvn spring-boot:run
```

Access swagger ui here:
```
http://localhost:8081/api/swagger-ui.html
```

Access h2 console here:
```
http://localhost:8081/api/manage/h2/console
```

Configuration is the following:
* **jdbc url:** jdbc:h2:mem:paramlocaldb
* **username:** sa
* **password:** password

#### Test

Launch all tests:
```bash
mvn test
```

Launch unit tests:
```bash
mvn test -DskipITs=true
```

Launch integration tests:
```bash
mvn verify -DskipUTs=true
```

#### Formatting

Check prettier formatting rules:
```bash
mvn prettier:check
```

Enforce prettier formatting rules:
```bash
mvn prettier:write
```

NOTE: Prettier is configured to be triggered uring the maven **validate** goal.
