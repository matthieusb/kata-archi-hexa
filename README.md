# Kata hexagonal architecture

This project was created to practise hexagonal architecture.
Have a look at each sub-project for more detail on how to access database and 

**All modules can be used as stand-alone projects**

### Building/Launching/Testing

#### Build

Build all projects and their tests:

```bash
mvn clean test-compile
```

#### Launch

Launch the main rest api:
```bash
mvn spring-boot:run
```

#### Test

Launch all tests:
```bash
mvn test
```

Launch unit tests:
```bash
mvn test -DskipITs=true
```

Launch integration tests:
```bash
mvn verify -DskipUTs=true
```

#### Formatting

Check prettier formatting rules:
```bash
mvn prettier:check
```

Enforce prettier formatting rules:
```bash
mvn prettier:write
```

NOTE: Prettier is configured to be triggered uring the maven **validate** goal.
