#!/bin/bash
# File used as a prepare-commit-message hook through the maven plugin
# Runs maven validate before commiting, to ensure prettier formatting is enforced on each commit

echo "Running Maven validate"

# -- Retrieving git repository home location
echo "Retrieving home folder for current git repository"
MAIN_DIR="$( git rev-parse --show-toplevel )"
MAIN_POM="$MAIN_DIR/pom.xml"

echo "Main pom file found as $MAIN_POM"
cd ${MAIN_DIR}

# -- Running maven validate
echo "Executing maven validate goal for $MAIN_POM"
mvn clean validate -f ${MAIN_POM}

if [[ $? -ne 0 ]]; then
  "Error while validating the code"
  exit 1
fi

exit 0
