package com.kata.hexago.domain.entity;

import java.util.UUID;

public class PizzaDoughType {
    private final UUID id;
    private final String label;
    private final String description;
    private final Thickness thickness;
    private final Price price;

    // TODO Add Validation checks

    public static PizzaDoughType create(UUID id, String label, String description, Thickness thickness, Price price) {
        return new PizzaDoughType(id, label, description, thickness, price);
    }

    public PizzaDoughType(UUID id, String label, String description, Thickness thickness, Price price) {
        this.id = id;
        this.label = label;
        this.description = description;
        this.thickness = thickness;
        this.price = price;
    }

    public UUID getId() {
        return id;
    }

    public String getLabel() {
        return label;
    }

    public String getDescription() {
        return description;
    }

    public Thickness getThickness() {
        return thickness;
    }

    public Price getPrice() {
        return price;
    }
}
