package com.kata.hexago.domain.service;

import com.kata.hexago.domain.entity.PizzaDoughType;
import com.kata.hexago.domain.repository.PizzaDoughRepositoryPort;
import io.vavr.collection.Seq;

public class PizzaDoughTypeService implements PizzaDoughTypeServicePort {
    private final PizzaDoughRepositoryPort pizzaDoughRepository;

    public PizzaDoughTypeService(PizzaDoughRepositoryPort pizzaDoughRepository) {
        this.pizzaDoughRepository = pizzaDoughRepository;
    }

    @Override
    public Seq<PizzaDoughType> getAllPizzaDoughTypes() {
        return pizzaDoughRepository.findAllPizzaDoughTypes();
    }
}
