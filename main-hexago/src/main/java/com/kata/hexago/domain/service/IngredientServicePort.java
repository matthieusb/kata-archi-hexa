package com.kata.hexago.domain.service;

import com.kata.hexago.domain.entity.Ingredient;
import com.patternity.annotation.ddd.stereotype.Port;
import io.vavr.collection.Seq;

@Port // primary
public interface IngredientServicePort {
    Seq<Ingredient> getAllIngredients();
}
