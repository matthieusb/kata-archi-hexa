package com.kata.hexago.domain.service;

import com.kata.hexago.domain.entity.Ingredient;
import com.kata.hexago.domain.repository.IngredientRepositoryPort;
import io.vavr.collection.Seq;

public class IngredientService implements IngredientServicePort {
    IngredientRepositoryPort ingredientRepository;

    public IngredientService(IngredientRepositoryPort ingredientRepository) {
        this.ingredientRepository = ingredientRepository;
    }

    @Override
    public Seq<Ingredient> getAllIngredients() {
        return ingredientRepository.getAllIngredients();
    }
}
