package com.kata.hexago.domain.entity;

import java.util.UUID;

public class Ingredient {
    final UUID id;
    final String label;
    final String description;
    final Price price;

    public Ingredient(UUID id, String label, String description, Price price) {
        // TODO Add validation on construction (Using vavr API ?)
        this.id = id;
        this.label = label;
        this.description = description;
        this.price = price;
    }

    public UUID getId() {
        return id;
    }

    public String getLabel() {
        return label;
    }

    public String getDescription() {
        return description;
    }

    public Price getPrice() {
        return price;
    }
}
