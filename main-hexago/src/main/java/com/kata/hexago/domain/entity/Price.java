package com.kata.hexago.domain.entity;

public class Price {
    final Double pricePerKilo;

    public Price(Double pricePerKilo) {
        this.pricePerKilo = pricePerKilo;
    }

    public Double getPricePerKilo() {
        return pricePerKilo;
    }
}
