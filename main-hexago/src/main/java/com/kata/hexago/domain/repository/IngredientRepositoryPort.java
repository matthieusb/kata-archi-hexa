package com.kata.hexago.domain.repository;

import com.kata.hexago.domain.entity.Ingredient;
import com.patternity.annotation.ddd.stereotype.Port;
import io.vavr.collection.Seq;

@Port // secondary
public interface IngredientRepositoryPort {
    Seq<Ingredient> getAllIngredients();
}
