package com.kata.hexago.domain.service;

import com.kata.hexago.domain.entity.PizzaDoughType;
import io.vavr.collection.Seq;

public interface PizzaDoughTypeServicePort {
    Seq<PizzaDoughType> getAllPizzaDoughTypes();
}
