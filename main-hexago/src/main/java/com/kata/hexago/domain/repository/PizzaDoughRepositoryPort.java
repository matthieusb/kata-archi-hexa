package com.kata.hexago.domain.repository;

import com.kata.hexago.domain.entity.PizzaDoughType;
import io.vavr.collection.Seq;

public interface PizzaDoughRepositoryPort {
    Seq<PizzaDoughType> findAllPizzaDoughTypes();
}
