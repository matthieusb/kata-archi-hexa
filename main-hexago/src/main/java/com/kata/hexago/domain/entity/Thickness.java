package com.kata.hexago.domain.entity;

public enum Thickness {
    THIN,
    THICK,
    EXTRA_THICK
}
