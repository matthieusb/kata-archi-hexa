package com.kata.hexago.infrastructure.presentation.rest.dto;

import com.kata.hexago.domain.entity.PizzaDoughType;
import java.util.UUID;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@AllArgsConstructor
@NoArgsConstructor
public class PizzaDoughTypeDto {
    private UUID id;
    private String label;
    private String description;
    private ThicknessDto thickness;
    private Double pricePerKilo;

    public static PizzaDoughTypeDto from(PizzaDoughType pizzaDoughType) {
        return new PizzaDoughTypeDto(
            pizzaDoughType.getId(),
            pizzaDoughType.getLabel(),
            pizzaDoughType.getDescription(),
            ThicknessDto.from(pizzaDoughType.getThickness()),
            pizzaDoughType.getPrice().getPricePerKilo()
        );
    }

    public UUID getId() {
        return id;
    }

    public String getLabel() {
        return label;
    }

    public String getDescription() {
        return description;
    }

    public ThicknessDto getThickness() {
        return thickness;
    }

    public Double getPricePerKilo() {
        return pricePerKilo;
    }
}
