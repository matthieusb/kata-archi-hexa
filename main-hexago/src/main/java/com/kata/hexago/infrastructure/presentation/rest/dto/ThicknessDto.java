package com.kata.hexago.infrastructure.presentation.rest.dto;

import com.kata.hexago.domain.entity.Thickness;

public enum ThicknessDto {
    THIN,
    THICK,
    EXTRA_THICK;

    public static ThicknessDto from(Thickness thickness) {
        return ThicknessDto.valueOf(thickness.name());
    }
}
