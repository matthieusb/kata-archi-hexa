package com.kata.hexago.infrastructure;

import com.kata.hexago.domain.entity.Ingredient;
import com.kata.hexago.domain.entity.PizzaDoughType;
import com.kata.hexago.domain.entity.Price;
import com.kata.hexago.domain.entity.Thickness;
import java.util.UUID;

public class Stubs {
    // FIXME Temporary constants

    // -- Common
    public static final Double pricePerKiloOne = 10.2D;
    public static final Double pricePerKiloTwo = 1.3D;

    public static final Price priceOne = new Price(pricePerKiloOne);
    public static final Price priceTwo = new Price(pricePerKiloTwo);

    // -- Ingredients
    public static final UUID ingredientUuidOne = UUID.fromString("dfb767a7-e193-4718-8a63-1bb9941570a0");
    public static final String ingredientLabelOne = "mozza di bufala";
    public static final String ingredientDescriptionOne = "italian mozarella di bufala";

    public static final UUID ingredientUuidTwo = UUID.fromString("dfb767a8-e193-4718-8a63-1bb9941570a0");
    public static final String ingredientLabelTwo = "mushrooms";
    public static final String ingredientDescriptionTwo = "standard mushrooms";

    public static final UUID ingredientUuidNotFound = UUID.fromString("dfb267a7-e193-4718-8a63-1bb994151222");

    public static final Ingredient ingredientOne = new Ingredient(
        ingredientUuidOne,
        ingredientLabelOne,
        ingredientDescriptionOne,
        priceOne
    );
    public static final Ingredient ingredientTwo = new Ingredient(
        ingredientUuidTwo,
        ingredientLabelTwo,
        ingredientDescriptionTwo,
        priceTwo
    );
    public static final Ingredient ingredientAbsentUuidFromParam = new Ingredient(
        ingredientUuidNotFound,
        "dummy",
        "dummy" + " ingredient with no param price",
        new Price(0.0D)
    );

    // -- Pizza
    public static final UUID pizzaDoughTypeUuidOne = UUID.fromString("dfb767a7-e193-4718-8b63-1bb9941570a0");
    public static final String pizzaDoughTypeLabelOne = "Classic Wheat Dough";
    public static final String pizzaDoughTypeDescriptionOne = "The classic dough will please most pizza lovers";

    public static final UUID pizzaDoughTypeUuidTwo = UUID.fromString("dfb767a7-e193-4718-8b63-1bb9941570a1");
    public static final String pizzaDoughTypeLabelTwo = "Gluten Free Dough";
    public static final String pizzaDoughTypeDescriptionTwo = "Gluten free for those with an intolerance";

    public static UUID pizzaDoughTypeUuidNotFound = UUID.fromString("dfb767a7-e192-4818-8b63-1bb9941570a1");

    public static final PizzaDoughType pizzaDoughTypeOne = PizzaDoughType.create(
        pizzaDoughTypeUuidOne,
        pizzaDoughTypeLabelOne,
        pizzaDoughTypeDescriptionOne,
        Thickness.THICK,
        priceOne
    );
    public static final PizzaDoughType pizzaDoughTypeTwo = PizzaDoughType.create(
        pizzaDoughTypeUuidTwo,
        pizzaDoughTypeLabelTwo,
        pizzaDoughTypeDescriptionTwo,
        Thickness.THIN,
        priceTwo
    );
}
