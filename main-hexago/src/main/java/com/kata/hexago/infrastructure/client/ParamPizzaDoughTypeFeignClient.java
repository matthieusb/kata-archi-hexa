package com.kata.hexago.infrastructure.client;

import com.kata.hexago.infrastructure.client.dto.PizzaDoughTypePriceDto;
import java.util.Optional;
import java.util.UUID;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(value = "param-pizza-dough-type", url = "http://localhost:8081/api/", decode404 = true)
public interface ParamPizzaDoughTypeFeignClient {
    @GetMapping(value = "/pizza/dough-type/{id}/price")
    Optional<PizzaDoughTypePriceDto> getPizzaDoughTypePricePerKilo(@PathVariable("id") UUID pizzaDoughTypeId);
}
