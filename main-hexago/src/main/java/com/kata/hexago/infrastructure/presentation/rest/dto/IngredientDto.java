package com.kata.hexago.infrastructure.presentation.rest.dto;

import com.kata.hexago.domain.entity.Ingredient;
import java.util.UUID;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@AllArgsConstructor
@NoArgsConstructor
public class IngredientDto {
    UUID id;
    String label;
    String description;
    Double pricePerKilo;

    public static IngredientDto from(Ingredient ingredient) {
        return new IngredientDto(
            ingredient.getId(),
            ingredient.getLabel(),
            ingredient.getDescription(),
            ingredient.getPrice().getPricePerKilo()
        );
    }
}
