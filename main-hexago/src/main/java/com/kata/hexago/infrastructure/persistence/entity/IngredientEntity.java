package com.kata.hexago.infrastructure.persistence.entity;

import java.util.UUID;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
@AllArgsConstructor
public class IngredientEntity {
    private UUID id;
    private String label;
    private String description;
}
