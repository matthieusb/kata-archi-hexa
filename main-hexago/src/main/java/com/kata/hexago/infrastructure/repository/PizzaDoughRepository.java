package com.kata.hexago.infrastructure.repository;

import com.kata.hexago.domain.entity.PizzaDoughType;
import com.kata.hexago.domain.entity.Thickness;
import com.kata.hexago.domain.repository.PizzaDoughRepositoryPort;
import com.kata.hexago.infrastructure.Stubs;
import com.kata.hexago.infrastructure.client.ParamPizzaDoughTypeFeignClient;
import com.kata.hexago.infrastructure.client.dto.PizzaDoughTypePriceDto;
import com.kata.hexago.infrastructure.persistence.entity.PizzaDoughTypeEntity;
import com.patternity.annotation.designpattern.Adapter;
import io.vavr.collection.Array;
import io.vavr.collection.Seq;
import io.vavr.control.Option;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;

@Adapter // secondary
@Repository
@RequiredArgsConstructor
public class PizzaDoughRepository implements PizzaDoughRepositoryPort {
    private final ParamPizzaDoughTypeFeignClient pizzaDoughTypeFeignClient;

    // TODO Inject jpa repository once issue 14 is done

    @Override
    public Seq<PizzaDoughType> findAllPizzaDoughTypes() {
        Seq<PizzaDoughTypeEntity> pizzaDoughTypeEntities = Array.of(
            new PizzaDoughTypeEntity(
                Stubs.pizzaDoughTypeUuidOne,
                Stubs.pizzaDoughTypeLabelOne,
                Stubs.pizzaDoughTypeDescriptionOne,
                Thickness.THICK
            ),
            new PizzaDoughTypeEntity(
                Stubs.pizzaDoughTypeUuidTwo,
                Stubs.pizzaDoughTypeLabelTwo,
                Stubs.pizzaDoughTypeDescriptionTwo,
                Thickness.THIN
            ),
            new PizzaDoughTypeEntity(
                Stubs.pizzaDoughTypeUuidNotFound,
                "whatevsLabel",
                "whatevDescription",
                Thickness.THICK
            )
        );

        return pizzaDoughTypeEntities.flatMap(
            pizzaDoughTypeEntity ->
                Option
                    .ofOptional(pizzaDoughTypeFeignClient.getPizzaDoughTypePricePerKilo(pizzaDoughTypeEntity.getId()))
                    .map(
                        pizzaDoughTypePriceDto ->
                            new PizzaDoughType(
                                pizzaDoughTypeEntity.getId(),
                                pizzaDoughTypeEntity.getLabel(),
                                pizzaDoughTypeEntity.getDescription(),
                                pizzaDoughTypeEntity.getThickness(), // TODO Change this to map from actual entity enum
                                PizzaDoughTypePriceDto.to(pizzaDoughTypePriceDto)
                            )
                    )
        );
    }
}
