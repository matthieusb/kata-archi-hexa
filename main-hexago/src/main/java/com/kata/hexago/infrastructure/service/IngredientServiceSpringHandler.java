package com.kata.hexago.infrastructure.service;

import com.kata.hexago.domain.repository.IngredientRepositoryPort;
import com.kata.hexago.domain.service.IngredientService;
import org.springframework.stereotype.Service;

@Service
public class IngredientServiceSpringHandler extends IngredientService {

    public IngredientServiceSpringHandler(IngredientRepositoryPort ingredientRepository) {
        super(ingredientRepository);
    }
}
