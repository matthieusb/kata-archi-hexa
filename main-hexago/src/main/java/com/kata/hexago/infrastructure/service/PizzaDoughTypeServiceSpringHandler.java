package com.kata.hexago.infrastructure.service;

import com.kata.hexago.domain.repository.PizzaDoughRepositoryPort;
import com.kata.hexago.domain.service.PizzaDoughTypeService;
import org.springframework.stereotype.Service;

@Service
public class PizzaDoughTypeServiceSpringHandler extends PizzaDoughTypeService {

    public PizzaDoughTypeServiceSpringHandler(PizzaDoughRepositoryPort pizzaDoughRepository) {
        super(pizzaDoughRepository);
    }
}
