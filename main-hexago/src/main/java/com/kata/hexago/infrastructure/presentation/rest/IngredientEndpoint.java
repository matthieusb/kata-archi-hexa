package com.kata.hexago.infrastructure.presentation.rest;

import com.kata.hexago.domain.service.IngredientServicePort;
import com.kata.hexago.infrastructure.presentation.rest.dto.IngredientDto;
import com.patternity.annotation.designpattern.Adapter;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@Adapter // primary
@RestController
@RequiredArgsConstructor
public class IngredientEndpoint {
    final IngredientServicePort ingredientService;

    @GetMapping("/ingredients")
    List<IngredientDto> getAllIngredients() {
        return ingredientService.getAllIngredients().map(IngredientDto::from).toJavaList();
    }
}
