package com.kata.hexago.infrastructure.client.dto;

import com.kata.hexago.domain.entity.Price;
import java.util.UUID;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
@AllArgsConstructor
public class IngredientPriceDto {
    private UUID ingredientId;
    private Double pricePerKilo;

    public static Price to(IngredientPriceDto ingredientPriceDto) {
        return new Price(ingredientPriceDto.pricePerKilo);
    }
}
