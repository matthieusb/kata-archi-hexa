package com.kata.hexago.infrastructure.client.dto;

import com.kata.hexago.domain.entity.PizzaDoughType;
import com.kata.hexago.domain.entity.Price;
import java.util.UUID;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
@AllArgsConstructor
public class PizzaDoughTypePriceDto {
    private UUID id;
    private Double pricePerKilo;

    public static Price to(PizzaDoughTypePriceDto pizzaDoughTypePriceDto) {
        return new Price(pizzaDoughTypePriceDto.getPricePerKilo());
    }
}
