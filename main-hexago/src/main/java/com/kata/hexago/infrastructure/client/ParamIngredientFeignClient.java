package com.kata.hexago.infrastructure.client;

import com.kata.hexago.infrastructure.client.dto.IngredientPriceDto;
import java.util.Optional;
import java.util.UUID;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(value = "param-ingredient", url = "http://localhost:8081/api/", decode404 = true)
public interface ParamIngredientFeignClient {
    @GetMapping(value = "/ingredient/{id}/price")
    Optional<IngredientPriceDto> getIngredientPricePerKilo(@PathVariable("id") UUID ingredientId);
}
