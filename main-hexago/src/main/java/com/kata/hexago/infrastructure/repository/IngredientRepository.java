package com.kata.hexago.infrastructure.repository;

import com.kata.hexago.domain.entity.Ingredient;
import com.kata.hexago.domain.repository.IngredientRepositoryPort;
import com.kata.hexago.infrastructure.Stubs;
import com.kata.hexago.infrastructure.client.ParamIngredientFeignClient;
import com.kata.hexago.infrastructure.client.dto.IngredientPriceDto;
import com.kata.hexago.infrastructure.persistence.entity.IngredientEntity;
import io.vavr.collection.Array;
import io.vavr.collection.Seq;
import io.vavr.control.Option;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;

@Repository
@RequiredArgsConstructor
public class IngredientRepository implements IngredientRepositoryPort {
    final ParamIngredientFeignClient ingredientFeignClient;

    // TODO Inject Jpa repository once it is available to get ingredients (See issue #14)

    @Override
    public Seq<Ingredient> getAllIngredients() {
        Seq<IngredientEntity> ingredientsEntities = Array.of( // TODO Replace this with jpa repository call
            new IngredientEntity(Stubs.ingredientUuidOne, Stubs.ingredientLabelOne, Stubs.ingredientDescriptionOne),
            new IngredientEntity(Stubs.ingredientUuidTwo, Stubs.ingredientLabelTwo, Stubs.ingredientDescriptionTwo),
            new IngredientEntity(Stubs.ingredientUuidNotFound, "whatevsLabel", "whatevsDescription")
        );

        return ingredientsEntities.flatMap(
            ingredientEntity ->
                Option
                    .ofOptional(ingredientFeignClient.getIngredientPricePerKilo(ingredientEntity.getId()))
                    .map(
                        ingredientPriceDto ->
                            new Ingredient(
                                ingredientEntity.getId(),
                                ingredientEntity.getLabel(),
                                ingredientEntity.getDescription(),
                                IngredientPriceDto.to(ingredientPriceDto)
                            )
                    )
        );
    }
}
