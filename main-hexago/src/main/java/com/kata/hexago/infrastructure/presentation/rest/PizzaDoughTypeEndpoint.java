package com.kata.hexago.infrastructure.presentation.rest;

import com.kata.hexago.domain.service.PizzaDoughTypeServicePort;
import com.kata.hexago.infrastructure.presentation.rest.dto.PizzaDoughTypeDto;
import com.patternity.annotation.designpattern.Adapter;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@Adapter // primary
@RestController
@RequiredArgsConstructor
public class PizzaDoughTypeEndpoint {
    final PizzaDoughTypeServicePort pizzaDoughTypeService;

    @GetMapping("pizza-dough-types")
    public List<PizzaDoughTypeDto> getPizzaDoughTypes() {
        return pizzaDoughTypeService.getAllPizzaDoughTypes().map(PizzaDoughTypeDto::from).toJavaList();
    }
}
