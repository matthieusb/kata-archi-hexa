package com.kata.hexago.infrastructure.persistence.entity;

import com.kata.hexago.domain.entity.Thickness;
import java.util.UUID;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
@AllArgsConstructor
public class PizzaDoughTypeEntity {
    private UUID id;
    private String label;
    private String description;
    private Thickness thickness; // TODO Change this to match an entity rather than a domain object
}
