package com.kata.hexago;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class MainHexagoApplication {

    public static void main(String[] args) {
        SpringApplication.run(MainHexagoApplication.class, args);
    }
}
