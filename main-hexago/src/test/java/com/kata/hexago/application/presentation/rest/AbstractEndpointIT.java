package com.kata.hexago.application.presentation.rest;

import java.net.MalformedURLException;
import java.net.URI;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public abstract class AbstractEndpointIT {
    @LocalServerPort
    int randomServerPort;

    URI baseApiUri;

    @BeforeEach
    void setUp() throws MalformedURLException {
        URI baseUri = URI.create("http://localhost:" + randomServerPort);
        baseApiUri = baseUri.resolve(baseUri.getPath() + "/api");
    }
}
