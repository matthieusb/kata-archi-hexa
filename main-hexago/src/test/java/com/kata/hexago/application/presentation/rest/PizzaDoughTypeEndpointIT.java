package com.kata.hexago.application.presentation.rest;

import static io.restassured.RestAssured.given;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.tuple;
import static org.mockito.Mockito.when;

import com.kata.hexago.domain.entity.PizzaDoughType;
import com.kata.hexago.domain.service.PizzaDoughTypeServicePort;
import com.kata.hexago.infrastructure.Stubs;
import com.kata.hexago.infrastructure.presentation.rest.dto.PizzaDoughTypeDto;
import com.kata.hexago.infrastructure.presentation.rest.dto.ThicknessDto;
import io.vavr.collection.Array;
import io.vavr.collection.Seq;
import java.util.Arrays;
import java.util.List;
import org.apache.http.HttpStatus;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.mock.mockito.MockBean;

class PizzaDoughTypeEndpointIT extends AbstractEndpointIT {
    @MockBean
    PizzaDoughTypeServicePort pizzaDoughTypeService;

    @Test
    void should_not_find_any_pizza_dough_types() {
        // Given
        when(pizzaDoughTypeService.getAllPizzaDoughTypes()).thenReturn(Array.empty());

        // When
        List<PizzaDoughTypeDto> pizzaDoughTypesReturned = Arrays.asList(
            given()
                .baseUri(baseApiUri.toString())
                .get("/pizza-dough-types")
                .then()
                .statusCode(HttpStatus.SC_OK)
                .extract()
                .as(PizzaDoughTypeDto[].class)
        );

        // Then
        assertThat(pizzaDoughTypesReturned).isEmpty();
    }

    @Test
    void should_find_pizza_dough_types() {
        // Given
        Seq<PizzaDoughType> pizzaDoughTypesFound = Array.of(Stubs.pizzaDoughTypeOne, Stubs.pizzaDoughTypeTwo);
        when(pizzaDoughTypeService.getAllPizzaDoughTypes()).thenReturn(pizzaDoughTypesFound);

        // When
        List<PizzaDoughTypeDto> pizzaDoughTypesReturned = Arrays.asList(
            given()
                .baseUri(baseApiUri.toString())
                .get("/pizza-dough-types")
                .then()
                .statusCode(HttpStatus.SC_OK)
                .extract()
                .as(PizzaDoughTypeDto[].class)
        );

        // Then
        assertThat(pizzaDoughTypesReturned)
            .hasSize(2)
            .extracting("id", "label", "description", "thickness", "pricePerKilo")
            .containsExactlyInAnyOrder(
                tuple(
                    Stubs.pizzaDoughTypeUuidOne,
                    Stubs.pizzaDoughTypeLabelOne,
                    Stubs.pizzaDoughTypeDescriptionOne,
                    ThicknessDto.THICK,
                    Stubs.pricePerKiloOne
                ),
                tuple(
                    Stubs.pizzaDoughTypeUuidTwo,
                    Stubs.pizzaDoughTypeLabelTwo,
                    Stubs.pizzaDoughTypeDescriptionTwo,
                    ThicknessDto.THIN,
                    Stubs.pricePerKiloTwo
                )
            );
    }
}
