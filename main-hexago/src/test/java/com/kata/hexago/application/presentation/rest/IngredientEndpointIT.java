package com.kata.hexago.application.presentation.rest;

import static io.restassured.RestAssured.given;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.tuple;
import static org.mockito.Mockito.when;

import com.kata.hexago.domain.entity.Ingredient;
import com.kata.hexago.domain.service.IngredientServicePort;
import com.kata.hexago.infrastructure.Stubs;
import com.kata.hexago.infrastructure.presentation.rest.dto.IngredientDto;
import io.vavr.collection.Array;
import io.vavr.collection.Seq;
import java.util.Arrays;
import java.util.List;
import org.apache.http.HttpStatus;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.mock.mockito.MockBean;

class IngredientEndpointIT extends AbstractEndpointIT {
    @MockBean
    IngredientServicePort ingredientService;

    @Test
    void should_get_no_ingredients() {
        // Given
        when(ingredientService.getAllIngredients()).thenReturn(Array.empty());

        // When
        List<IngredientDto> ingredientsReturned = Arrays.asList(
            given()
                .baseUri(baseApiUri.toString())
                .get("/ingredients")
                .then()
                .statusCode(HttpStatus.SC_OK)
                .extract()
                .as(IngredientDto[].class)
        );

        // Then
        assertThat(ingredientsReturned).isEmpty();
    }

    @Test
    void should_get_all_ingredients() {
        // Given
        Seq<Ingredient> ingredientsFound = Array.of(Stubs.ingredientOne, Stubs.ingredientTwo);
        when(ingredientService.getAllIngredients()).thenReturn(ingredientsFound);

        // When
        List<IngredientDto> ingredientsReturned = Arrays.asList(
            given()
                .baseUri(baseApiUri.toString())
                .get("/ingredients")
                .then()
                .statusCode(HttpStatus.SC_OK)
                .extract()
                .as(IngredientDto[].class)
        );

        // Then
        assertThat(ingredientsReturned)
            .hasSize(2)
            .extracting("id", "label", "description", "pricePerKilo")
            .containsExactlyInAnyOrder(
                tuple(
                    Stubs.ingredientUuidOne,
                    Stubs.ingredientLabelOne,
                    Stubs.ingredientDescriptionOne,
                    Stubs.pricePerKiloOne
                ),
                tuple(
                    Stubs.ingredientUuidTwo,
                    Stubs.ingredientLabelTwo,
                    Stubs.ingredientDescriptionTwo,
                    Stubs.pricePerKiloTwo
                )
            );
    }
}
