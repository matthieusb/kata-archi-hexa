package com.kata.hexago;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class MainHexagoApplicationIT {

    @Test
    void contextLoads() {
        assertThat(true).isTrue();
    }
}
