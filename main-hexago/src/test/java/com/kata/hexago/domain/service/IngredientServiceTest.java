package com.kata.hexago.domain.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import com.kata.hexago.domain.entity.Ingredient;
import com.kata.hexago.domain.repository.IngredientRepositoryPort;
import io.vavr.collection.Array;
import io.vavr.collection.Seq;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
class IngredientServiceTest {
    @Mock
    IngredientRepositoryPort ingredientRepository;

    @InjectMocks
    IngredientService ingredientService;

    @Test
    void should_return_available_ingredients() {
        // Given
        Ingredient ingredientOne = mock(Ingredient.class);
        Ingredient ingredientTwo = mock(Ingredient.class);

        Seq<Ingredient> ingredients = Array.of(ingredientOne, ingredientTwo);
        when(ingredientRepository.getAllIngredients()).thenReturn(ingredients);

        // When
        Seq<Ingredient> ingredientsFound = ingredientService.getAllIngredients();

        // Then
        assertThat(ingredientsFound).hasSize(2);
    }
}
