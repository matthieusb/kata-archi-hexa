package com.kata.hexago.domain.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import com.kata.hexago.domain.entity.PizzaDoughType;
import com.kata.hexago.domain.repository.PizzaDoughRepositoryPort;
import io.vavr.collection.Array;
import io.vavr.collection.Seq;
import org.assertj.vavr.api.VavrAssertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
class PizzaDoughTypeServiceTest {
    @Mock
    PizzaDoughRepositoryPort pizzaRepository;

    @InjectMocks
    PizzaDoughTypeService pizzaDoughTypeService;

    @Test
    void should_not_find_pizza_dough_types() {
        // Given
        when(pizzaRepository.findAllPizzaDoughTypes()).thenReturn(Array.empty());

        // When Then
        VavrAssertions.assertThat(pizzaDoughTypeService.getAllPizzaDoughTypes()).isEmpty();
    }

    @Test
    void should_find_pizza_dough_types() {
        // Given
        PizzaDoughType pizzaDoughTypeOne = mock(PizzaDoughType.class);
        PizzaDoughType pizzaDoughTypeTwo = mock(PizzaDoughType.class);

        when(pizzaRepository.findAllPizzaDoughTypes()).thenReturn(Array.of(pizzaDoughTypeOne, pizzaDoughTypeTwo));

        // When
        Seq<PizzaDoughType> pizzaDoughTypeReturned = pizzaDoughTypeService.getAllPizzaDoughTypes();

        // Then
        VavrAssertions.assertThat(pizzaDoughTypeReturned).hasSize(2);
    }
}
