package com.kata.hexago.infrastructure.persistence.repository;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.tuple;
import static org.mockito.Mockito.when;

import com.kata.hexago.domain.entity.Ingredient;
import com.kata.hexago.infrastructure.Stubs;
import com.kata.hexago.infrastructure.client.ParamIngredientFeignClient;
import com.kata.hexago.infrastructure.client.dto.IngredientPriceDto;
import com.kata.hexago.infrastructure.repository.IngredientRepository;
import io.vavr.collection.Seq;
import java.util.Optional;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
class IngredientRepositoryTest {
    @Mock
    ParamIngredientFeignClient ingredientFeignClient;

    @InjectMocks
    IngredientRepository ingredientRepository;

    @Test
    void should_find_all_ingredients_with_their_correct_prices_and_filter_ingredients_without_prices_found() {
        // Given
        IngredientPriceDto ingredientPriceDtoOne = new IngredientPriceDto(
            Stubs.ingredientUuidOne,
            Stubs.pricePerKiloOne
        );
        IngredientPriceDto ingredientPriceDtoTwo = new IngredientPriceDto(
            Stubs.ingredientUuidTwo,
            Stubs.pricePerKiloTwo
        );

        when(ingredientFeignClient.getIngredientPricePerKilo(Stubs.ingredientUuidOne))
            .thenReturn(Optional.of(ingredientPriceDtoOne));
        when(ingredientFeignClient.getIngredientPricePerKilo(Stubs.ingredientUuidTwo))
            .thenReturn(Optional.of(ingredientPriceDtoTwo));
        when(ingredientFeignClient.getIngredientPricePerKilo(Stubs.ingredientUuidNotFound))
            .thenReturn(Optional.empty());

        // When
        Seq<Ingredient> ingredientsFound = ingredientRepository.getAllIngredients();

        // Then
        assertThat(ingredientsFound).hasSize(2);
        assertThat(ingredientsFound)
            .extracting("id", "price.pricePerKilo")
            .containsExactlyInAnyOrder(
                tuple(Stubs.ingredientUuidOne, Stubs.pricePerKiloOne),
                tuple(Stubs.ingredientUuidTwo, Stubs.pricePerKiloTwo)
            );
    }
}
