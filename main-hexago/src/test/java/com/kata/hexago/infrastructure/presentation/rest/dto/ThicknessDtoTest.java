package com.kata.hexago.infrastructure.presentation.rest.dto;

import static org.assertj.core.api.Assertions.assertThat;

import com.kata.hexago.domain.entity.Thickness;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import org.junit.jupiter.api.Test;

class ThicknessDtoTest {

    @Test
    void should_map_from_domain_enum() {
        List<ThicknessDto> thicknessDtos = Arrays
            .stream(Thickness.values())
            .map(ThicknessDto::from)
            .collect(Collectors.toList());

        assertThat(thicknessDtos).isNotEmpty().allMatch(Objects::nonNull);
    }
}
