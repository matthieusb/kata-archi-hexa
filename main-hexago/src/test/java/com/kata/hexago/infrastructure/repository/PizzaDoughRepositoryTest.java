package com.kata.hexago.infrastructure.repository;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.tuple;
import static org.mockito.Mockito.when;

import com.kata.hexago.domain.entity.PizzaDoughType;
import com.kata.hexago.infrastructure.Stubs;
import com.kata.hexago.infrastructure.client.ParamPizzaDoughTypeFeignClient;
import com.kata.hexago.infrastructure.client.dto.PizzaDoughTypePriceDto;
import io.vavr.collection.Seq;
import java.util.Optional;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
class PizzaDoughRepositoryTest {
    @Mock
    ParamPizzaDoughTypeFeignClient pizzaDoughTypeFeignClient;

    @InjectMocks
    PizzaDoughRepository pizzaDoughRepository;

    @Test
    void should_find_all_pizza_dough_types_with_their_correct_prices_and_filter_ingredients_wihtout_prices_found() {
        // Given
        PizzaDoughTypePriceDto pizzaDoughTypePriceDtoOne = new PizzaDoughTypePriceDto(
            Stubs.pizzaDoughTypeUuidOne,
            Stubs.pricePerKiloOne
        );
        PizzaDoughTypePriceDto pizzaDoughTypePriceDtoTwo = new PizzaDoughTypePriceDto(
            Stubs.pizzaDoughTypeUuidTwo,
            Stubs.pricePerKiloTwo
        );

        when(pizzaDoughTypeFeignClient.getPizzaDoughTypePricePerKilo(Stubs.pizzaDoughTypeUuidOne))
            .thenReturn(Optional.of(pizzaDoughTypePriceDtoOne));
        when(pizzaDoughTypeFeignClient.getPizzaDoughTypePricePerKilo(Stubs.pizzaDoughTypeUuidTwo))
            .thenReturn(Optional.of(pizzaDoughTypePriceDtoTwo));
        when(pizzaDoughTypeFeignClient.getPizzaDoughTypePricePerKilo(Stubs.pizzaDoughTypeUuidNotFound))
            .thenReturn(Optional.empty());

        // When
        Seq<PizzaDoughType> pizzaDoughTypesFound = pizzaDoughRepository.findAllPizzaDoughTypes();

        // Then
        assertThat(pizzaDoughTypesFound).hasSize(2);
        assertThat(pizzaDoughTypesFound)
            .extracting("id", "price.pricePerKilo")
            .containsExactlyInAnyOrder(
                tuple(Stubs.pizzaDoughTypeUuidOne, Stubs.pricePerKiloOne),
                tuple(Stubs.pizzaDoughTypeUuidTwo, Stubs.pricePerKiloTwo)
            );
    }
}
